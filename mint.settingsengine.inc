<?php

global $mint_conf;
$mint_conf = array();

function _mint_getconfig()
	{
	global $base_url;
	global $mint_conf;
	if(count($mint_conf) > 0) return $mint_conf;
	
	$defaults = array();
	$defaults["path"] = "/mint";
	$defaults["log_everything"] = FALSE;
	$defaults["secret_crush"] = FALSE;
	
	$conf = array();
	foreach($defaults as $key => $value)
		{
		$conf[$key] = variable_get('mint_' . $key, $value);
		}
	
	$mint_conf = $conf;
	return $conf;
	}

function mint_admin_settings_validate($form, &$form_state)
	{
	if(trim($form_state['values']["mint_path"]) == "")
		{
		form_set_error('mint_path', t('A path to Mint is required. If you wish to disable Mint integration, please disable this module.'));
		}
	else //There is a path.
		{
		//Check that the Mint path is absolute.
		if(substr($form_state['values']["mint_path"], 0, 1) != "/" && substr($form_state['values']["mint_path"], 0, 7) != "http://" && substr($form_state['values']["mint_path"], 0, 8) != "https://")
			{
			form_set_error('mint_path', t("Your Mint path must begin with '/', 'http://', or 'https://'. Otherwise your Mint path is not absolute and we will not be able to find it."));
			}
		
		//Check that the Mint path has no trailing slash.
		if(substr($form_state['values']["mint_path"], -1) == "/")
			{
			form_set_error('mint_path', t("Your Mint path may not end with a trailing slash."));
			}
		}
	}

function mint_admin_settings_submit($form, &$form_state)
	{
	cache_clear_all('*', 'cache_page', TRUE);
	drupal_set_message(t('The page cache was cleared.'), 'status');
	}

function mint_admin_settings()
	{
	$conf = _mint_getconfig();
	
	$form['mint_info'] = array('#value' => '<p>' . t('On this page, you can configure how we integrate with Mint. If these settings are correct, you may also <a href="@mintpath">visit your Mint</a>.', array('@mintpath' => $conf['path'])) . '</p>');
	
	//Fix the validation madness.
	$form['#validate'][] = 'mint_admin_settings_validate';
	$form['#submit'][] = 'mint_admin_settings_submit';
	
	$form['mint_path'] = array
		(
		'#type' => 'textfield',
		'#title' => t('Where is Mint?'),
		'#default_value' => $conf["path"],
		'#size' => 45,
		'#maxlength' => 64,
		'#required' => TRUE,
		'#description' => t("What is the path to Mint's Javascript-based hit logger? If possible, this should be a relative path from Drupal's root.")
		);
	
	$form['mint_log_everything'] = array
		(
		'#type' => 'checkbox',
		'#title' => t('Log EVERYTHING?'),
		'#default_value' => $conf["log_everything"],
		'#description' => t('By default, we exclude the site admin and anyone else with the "exclude from mint" privilege. If you want us to log EVERYTHING, check this box.')
		);
	
	$fieldset = array
		(
		'#type' => 'fieldset',
		'#title' => t('Pepper Options'),
		'#collapsible' => FALSE
		);

	$fieldset['mint_secret_crush'] = array
		(
		'#type' => 'checkbox',
		'#title' => t('Track Users with Secret Crush?'),
		'#default_value' => $conf["secret_crush"],
		'#description' => t("Do you want to allow tracking of users with the 'Secret Crush' pepper? This works by setting a cookie with the user's username in plain text. (Note that this could be seen as a security risk.) To make this work, you will need to enable the Secret Crush pepper in Mint and set the 'cookie name' to 'mint_crush_user' in the preferences of the Secret Crush pepper.")
		);

	$form['mint_peppers'] = $fieldset;
	
	return system_settings_form($form);
	}

